import os
#import django_heroku
from decouple import config, Csv
from environs import Env
from dotenv import load_dotenv, find_dotenv

env = Env()
env.read_env()
load_dotenv(find_dotenv())

######################
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'AUTH_SOURCE': config('DB_NAME'),
        'NAME': config('DB_NAME'),
        'USER': config('DB_USER'),
        'PASSWORD': config('DB_PASSWORD'),
        'HOST': config('DB_HOST'),
        'PORT': config('DB_PORT', cast=int),
    }
}

# # Heroku: Update database configuration from $DATABASE_URL.
import dj_database_url
db_from_env = dj_database_url.config(conn_max_age=500)
DATABASES['default'].update(db_from_env)


# Akismet API keys

###################
AKISMET_API_KEY = config('PYTHON_AKISMET_API_KEY')

AKISMET_SITE_URL = env.list("PYTHON_AKISMET_BLOG_URL", subcast=str)

AKISMET_BLOG_URL = env.list("PYTHON_AKISMET_BLOG_URL", subcast=str)

AKISMET_TEST_MODE = config('PYTHON_AKISMET_TEST_MODE')

RECAPTCHA_PUBLIC_KEY = config('RECAPTCHA_PUBLIC_KEY', cast=str)

RECAPTCHA_PRIVATE_KEY = config('RECAPTCHA_PRIVATE_KEY', cast=str)

#RECAPTCHA_WIDGET = config('RECAPTCHA_WIDGET')

#RECAPTCHA_TIMEOUT = config('RECAPTCHA_TIMEOUT', cast=int) #5

#RECAPTCHA_PASS_ON_ERROR = config('RECAPTCHA_PASS_ON_ERROR', cast=bool)#False

#######################


SECURE_HSTS_SECONDS = 60

SECURE_CONTENT_TYPE_NOSNIFF = True

SECURE_BROWSER_XSS_FILTER = True

SESSION_COOKIE_SECURE = True

CSRF_COOKIE_SECURE = True

X_FRAME_OPTIONS = 'DENY'

SECURE_HSTS_INCLUDE_SUBDOMAINS = True

SECURE_HSTS_PRELOAD = True

###############################

# AnyMail Mailgun

ANYMAIL = {
    'MAILGUN_API_KEY': config('MAILGUN_API_KEY', cast=str),
    'MAILGUN_SENDER_DOMAIN': config('MAIL_SENDER_DOMAIN', cast=str),
    #'MAILGUN_API_URL': config('MAILGUN_API_URL', cast=str),   
}

EMAIL_BACKEND = config('EMAIL_BACKEND', cast=str)
DEFAULT_FROM_EMAIL = config('DEFAULT_FROM_EMAIL', cast=str)
SERVER_EMAIL = config('SERVER_EMAIL', cast=str)