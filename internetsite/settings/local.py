import os
from decouple import config, Csv
from environs import Env
from dotenv import load_dotenv, find_dotenv


env = Env()
env.read_env()
load_dotenv(find_dotenv())

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'AUTH_SOURCE': config('DB_NAME'),
        'NAME': config('DB_NAME'),
        'USER': config('DB_USER'),
        'PASSWORD': config('DB_PASSWORD'),
        'HOST': config('DB_HOST'),
        'PORT': config('DB_PORT', cast=int),
    }
}


# #Email SMTP server

# DEFAULT_FROM_EMAIL = env.str("DEFAULT_FROM_EMAIL")

# EMAIL_USE_TLS = env.bool("EMAIL_USE_TLS")
# EMAIL_HOST = env.str("EMAIL_HOST")
# EMAIL_HOST_USER = env.str("EMAIL_HOST_USER")
# EMAIL_HOST_PASSWORD = env.str("EMAIL_HOST_PASSWORD")
# EMAIL_PORT= env.int("EMAIL_PORT")
EMAIL_RECEIVER = config('EMAIL_RECEIVER')


ADMINS = (
    ('Arnel Imperial', EMAIL_RECEIVER),  
)

MANAGERS = ADMINS



