from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from internetsite.settings import WINDOWS, WINDOWSOTP
from django.views.generic import TemplateView, RedirectView
from django.http import HttpResponseRedirect
from users.models import User as UserModel
from django.conf.urls import handler500
from pages import views as pages
from users.admin import admin_log
from django.contrib.auth.views import LoginView
from pages.views import IconView


urlpatterns = [
    #path(WINDOWS, admin.site.urls),
    path(WINDOWSOTP, admin_log.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', include('pages.urls')),
    path('draft/', include('draft.urls')),
    path('robots.txt', TemplateView.as_view(template_name="robots.txt", content_type='text/plain')),
    path('sitemap.xml', TemplateView.as_view(template_name="sitemap.xml", content_type='text/xml')),
    path('humans.txt', TemplateView.as_view(template_name="humans.txt", content_type='text/plain')),
    #path('favicon.ico', IconView.as_view(), name='icon'),
    #path('favicon.ico', RedirectView.as_view(url=settings.STATIC_URL + 'favicon.ico')),
    path('favicon.ico', lambda x: HttpResponseRedirect(settings.STATIC_URL + 'img/icons/48/favicon.ico')),
    path('favicon.ico', lambda x: HttpResponseRedirect(settings.STATIC_URL + 'img/icons/96/favicon.ico')),
    path('favicon.ico', lambda x: HttpResponseRedirect(settings.STATIC_URL + 'img/icons/128/favicon.ico')),
]


urlpatterns += staticfiles_urlpatterns()

handler500 = pages.err_500


