from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from .models import User
from phonenumber_field.formfields import PhoneNumberField
#from phonenumber_field.widgets import PhoneNumberPrefixWidget
from zxcvbn_password.fields import PasswordField, PasswordConfirmationField
class SignUpForm(UserCreationForm):
    username = forms.CharField(required=True, widget=forms.TextInput)
    email = forms.EmailField(required=True, widget=forms.EmailInput)
    phone = PhoneNumberField(required=True)
    first_name = forms.CharField(required=True, widget=forms.TextInput)
    last_name = forms.CharField(required=True, widget=forms.TextInput)
    password1 = PasswordField()
    password2 = PasswordConfirmationField(confirm_with='password1')
    
    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'phone', 'password1', 'password2')


class SignUpChangeForm(UserChangeForm):
    username = forms.CharField(required=True, widget=forms.TextInput)
    email = forms.EmailField(required=True, widget=forms.EmailInput)
    phone = PhoneNumberField(required=True)
    first_name = forms.CharField(required=True, widget=forms.TextInput)
    last_name = forms.CharField(required=True, widget=forms.TextInput)

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'phone',)

