from django.db import models
from django.contrib.auth.models import AbstractUser
from phonenumber_field.modelfields import PhoneNumberField
from .managers import UserManager
from django.core.validators import validate_email

class User(AbstractUser):
    system_id = models.AutoField(primary_key=True, unique=True, blank=True)
    email = models.EmailField(unique=True, validators=[validate_email])
    phone = PhoneNumberField(unique=True, blank=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'first_name', 'last_name', 'phone']

    objects = UserManager()

    def __str__(self):
        return self.email