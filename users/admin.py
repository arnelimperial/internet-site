from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User as UserModel
from .forms import SignUpChangeForm, SignUpForm
from django_otp.admin import OTPAdminSite
from django_otp.plugins.otp_totp.models import TOTPDevice
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import Group, Permission
from django.contrib.sites.models import Site
from django.contrib.auth.models import Group, Permission
from django.contrib.auth.admin import GroupAdmin, UserAdmin
from draft.models import Draft
class OTPAdmin(OTPAdminSite):
    pass

admin_log = OTPAdmin(name='OTPAdmin')

class CustomAdmin(UserAdmin):
    add_form = SignUpForm
    form = SignUpChangeForm
    readonly_fields = ['system_id']
   
    fieldsets = (
        (None, {'fields': ('username', 'email', 'password', 'system_id')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'phone')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username','email', 'first_name','last_name','phone','password1', 'password2'),
        }),
    )
    list_display = ('username','email', 'first_name', 'last_name', 'is_staff')
    search_fields = ('username','email', 'first_name', 'last_name''phone')
    ordering = ('email',)

    # class Meta:
    #     model = UserModel

admin.site.register(UserModel, CustomAdmin)
#admin.site.register(TOTPDevice)
admin.site.register(Permission)
admin_log.register(UserModel, CustomAdmin)
admin_log.register(TOTPDevice)
admin_log.register(Group)
admin_log.register(Permission)
admin_log.register(Draft)


