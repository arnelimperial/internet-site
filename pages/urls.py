from .views import HomeView, AboutView, SendView, ContactView
from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('about/', AboutView.as_view(), name='about'),
    path('contact/', ContactView.as_view(), name='contact_form'),
    path('contact/sent/', SendView.as_view(), name='contact_form_sent'),
]