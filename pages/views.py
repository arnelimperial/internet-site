from django.shortcuts import render, redirect
from django.views.generic import TemplateView, ListView, DetailView
from .forms import ContactForm
from django.views.generic.edit import FormView
from django.urls import reverse
from django.core.mail import send_mail, BadHeaderError, send_mass_mail, EmailMultiAlternatives
from django.core import mail
from django.http import HttpResponse, HttpResponseRedirect, HttpRequest, HttpResponseForbidden
from django.core.exceptions import PermissionDenied
from django.conf import settings
import datetime
from datetime import date
from django.contrib import messages
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from internetsite.settings import production, local
from akismet import Akismet
from internetsite.settings import production
from draft.models import Draft



class HomeView(TemplateView):
    template_name = 'pages/main.html'

    def get_context_data(self, *args, **kwargs):
        context = super(HomeView, self).get_context_data(*args, **kwargs)
        month_object = datetime.datetime.now()
        month = month_object.strftime("%B")
        today = date.today()
        context['title'] = "Internet Site [Arnel Imperial - arnelimperial.com]"
        context['gtag'] = 'XGkUfqt3z01McIWECdHOckgGdTn7fgRAYZ4R8zsFowU'
        context['description'] = 'Home Page'
        return context

class AboutView(TemplateView):

    template_name = 'pages/about.html'

    def get_context_data(self, *args, **kwargs):
        context = super(AboutView, self).get_context_data(*args, **kwargs)
        context['title'] = 'About'
        context['description'] = "About Page"
        return context

receiver = getattr(settings, 'EMAIL_RECEIVER')
default_sender = getattr(settings, 'DEFAULT_FROM_EMAIL')

class ContactView(FormView):
    template_name = 'contact_form/contact_form.html'
    form_class = ContactForm

    def get(self, request):
        form = ContactForm()
        title = 'Contact'
        return render(request, self.template_name, {'form': form, 'title': title})

    def form_valid(self, form):
        name = form.cleaned_data['name']
        email = form.cleaned_data['email']
        body = form.cleaned_data['body']
        self.request.session['name'] = form.cleaned_data['name']
        self.request.session['email'] = form.cleaned_data['email']
        self.request.session['body'] = form.cleaned_data['body']
        
        html_content = render_to_string('contact_form/copy.html', {'name':name, 'email':email, 'body':body})
        text_content = strip_tags(html_content)
        message = '{}\n\nSender E-mail: {}\nSender Name: {}'.format(body, email, name)
        subject = 'You have a message from [ {} | {} ]'.format(name, email)
        subject_for_sender = 'Copy of your message to Arnel Imperial'
        # Send to Admin
        send_mail(subject, message, 'From arnelimperial.com <noreply@arnelimperial.com>', [receiver], fail_silently=False,)
        # Send copy to sender
        msg = EmailMultiAlternatives(subject_for_sender, text_content, 'From arnelimperial.com <noreply@arnelimperial.com>', [email])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
      
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        messages.add_message(self.request, messages.WARNING, 'Message submitted.')
        return reverse('contact_form_sent')

class SendView(TemplateView):

    template_name = 'contact_form/contact_form_sent.html'

    def get_context_data(self, *args, **kwargs):
        context = super(SendView, self).get_context_data(*args, **kwargs)
        context['title'] = 'Mail Sent'
        return context

def err_500(request):
    return render(request,'500.html')

class IconView(TemplateView):
    template_name = 'pages/icon.html'

    def get_context_data(self, *args, **kwargs):
        context = super(IconView, self).get_context_data(*args, **kwargs)
        context['title'] = 'Hi, I\'m Arnel'
        context['description'] = "Arnel Imperial\'s internet site icon."
        return context
