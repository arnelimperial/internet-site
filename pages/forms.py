from django import forms
from antispam.honeypot.forms import HoneypotField
from antispam import akismet
from django.template import loader
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from internetsite.settings import production
from internetsite.settings import local
from django.contrib import messages
from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Invisible, ReCaptchaV2Checkbox

public_key = getattr(settings, 'RECAPTCHA_PUBLIC_KEY')
private_key = getattr(settings, 'RECAPTCHA_PRIVATE_KEY')

class ContactForm(forms.Form):
    name = forms.CharField(label='', widget=forms.TextInput(attrs={'class':'rounded bg-dark text-light form-control-lg', 'placeholder': 'Your Name'}), error_messages={'required': 'Who are your?. Your name is required.'})
    email = forms.EmailField(label='', widget=forms.EmailInput(attrs={'class':'rounded bg-dark text-light form-control-lg', 'placeholder': 'Email Address'}), error_messages={'required': 'Don\'t worry I will not spam your email. Email field is currently empty.'})
    body = forms.CharField(label='', widget=forms.Textarea(attrs={'class':'rounded text-light bg-dark border-dark form-control-lg', 'placeholder': 'Message','style':'height:250px;width:500px;'}), error_messages={'required': 'Oops! Don\'t be in a hurry. Please type your message.'})
    spam_honeypot_field = HoneypotField()
    captcha = ReCaptchaField(label='', public_key=public_key, private_key=private_key, widget=ReCaptchaV2Invisible)
    
    def __init__(self, *args,**kwargs):
        self.request = kwargs.pop('request', None)

        super(ContactForm, self).__init__(*args, **kwargs)

    def clean(self):
        if self.request and akismet.check(
            request=akismet.Request.from_django_request(self.request),
            comment=akismet.Comment(
                content=self.cleaned_data['body'],
                type='comment',

                author=akismet.Author(
                    name=self.cleaned_data['name'],
                    email=self.cleaned_data['email']
                )
            )
        ):
            raise ValidationError('Spam detected', code='spam-protection')

        return super().clean()



# class AkismetContactForm(ContactForm):
    
#     def __init__(self, *args,**kwargs):
#         self.request = kwargs.pop('request', None)

#         super().__init__(*args, **kwargs)

#     def clean(self):
#         if self.request and akismet.check(
#             request=akismet.Request.from_django_request(self.request),
#             comment=akismet.Comment(
#                 content=self.cleaned_data['body'],
#                 type='comment',

#                 author=akismet.Author(
#                     name=self.cleaned_data['name'],
#                     email=self.cleaned_data['email']
#                 )
#             )
#         ):
#             raise ValidationError('Spam detected', code='spam-protection')

#         return super().clean()
