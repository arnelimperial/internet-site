from .views import DraftView, DraftDetailView, DraftCreateView, DraftUpdateView, DraftDeleteView
from django.urls import path, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('', DraftView.as_view(), name='draft_list'),
    path('<slug:slug>', DraftDetailView.as_view(), name='draft_detail'),
    path('create/', DraftCreateView.as_view(), name='draft_create'),
    path('<slug:slug>/update/', DraftUpdateView.as_view(), name='draft_update'),
    path('<slug:slug>/delete/', DraftDeleteView.as_view(), name='draft_delete'),
]