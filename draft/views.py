from django.shortcuts import render
from .models import Draft
from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.urls import reverse_lazy


class DraftView(ListView):
    model = Draft
    template_name = 'pages/draft.html'
    ordering = ['-updated_date']
    draft_lists = Draft.objects.all()
    context_object_name = 'draft_lists'

    def get_context_data(self, *args, **kwargs):
        context = super(DraftView, self).get_context_data(*args, **kwargs)
        context['title'] = 'Draft'
        context['description'] = 'Draft Page'
        return context

class DraftDetailView(DetailView):
    model = Draft
    template_name = 'draft/draft_detail.html'

class DraftCreateView(LoginRequiredMixin,CreateView):
    model = Draft
    template_name = 'draft/draft_form.html'
    fields = ['title', 'content', 'description', 'code', 'text']

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

class DraftUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Draft
    #template_name = 'draft/draft_form.html'
    fields = ['title', 'content', 'description', 'code', 'text']

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

    def test_func(self):
        draft = self.get_object()
        if self.request.user == draft.user:
            return True
        return False



class DraftDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Draft
    template_name = 'draft/draft_confirm_delete.html'
    success_url = reverse_lazy('draft_list')
    
    def test_func(self):
        draft = self.get_object()
        if self.request.user == draft.user:
            return True
        return False



