from django.db import models
from users.models import User
from internetsite.settings.base import AUTH_USER_MODEL
from django.utils import timezone
from django.urls import reverse
from django.template.defaultfilters import slugify
class Draft(models.Model):
    draft_id = models.AutoField(primary_key=True, unique=True, blank=True)
    user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='draft')
    slug = models.SlugField(max_length=300, unique=True)
    title = models.CharField(max_length=1024, unique=True)
    description = models.TextField(default='')
    content = models.TextField()
    code = models.TextField(blank=True, default='')
    text = models.TextField(blank=True, default='')
    created_date = models.DateTimeField(default=timezone.now)
    updated_date = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-updated_date']
    
    def __str__(self):
        return self.title

    
    def get_absolute_url(self):
        return reverse('draft_detail', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        return super().save(*args, **kwargs)



